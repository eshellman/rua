from .settings_dev import *

ALLOWED_HOSTS = [
    '127.0.0.1'
]

#Deactivates Boto
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
