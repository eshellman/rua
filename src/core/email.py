import mimetypes
import os

from django.conf import settings
from django.core.files.storage import default_storage
from django.core.mail import EmailMessage
from django.template import Context, Template, RequestContext
from django.urls import reverse

from core import log

from .util import get_setting


def create_email_message(
        subject,
        html_content,
        from_address,
        to,
        bcc,
        cc,
):
    """Create a EmailMessage, making sure it has an acceptable FROM address"""

    reply_to = from_address

    msg = EmailMessage(
        subject,
        html_content,
        from_address,
        to,
        bcc=bcc,
        cc=cc,
        reply_to=[reply_to],
    )

    msg.content_subtype = "html"

    return msg


def add_attachment(attachment, msg, proposal_id=None):
    """Adds an attachment to a EmailMessage object"""

    filepath = os.path.join(
        settings.EMAIL_DIR,
        str(attachment.uuid_filename),
    )

    with default_storage.open(filepath, 'rb') as file:
        content = file.read()

        mimetype = (
            mimetypes.guess_type(filepath)[0]
            or 'application/octet-stream'
        )

        msg.attach(
            filename=attachment.original_filename,
            content=content,
            mimetype=mimetype,
        )


def get_email_body(request, setting_name, context):
    """Renders an email body based on a template setting and context.

    Args:
        request(django.http.request.HttpRequest): request from the calling view.
        setting_name(str): name of the email setting used as a template.
        context(dict): context data to be substituted into the email body.

    Returns:
        str: the rendered email body.
    """
    html_template = get_setting(setting_name, 'email')
    html_template.replace('\n', '<br />')

    template_renderer = Template(html_template)

    con = RequestContext(request)
    con.push(context)
    html_content = template_renderer.render(con)

    return html_content


def get_email_subject(request, setting_name, context):
    """Renders an email subject based on a template setting and context.

    Args:
        request(django.http.request.HttpRequest): request from the calling view.
        setting_name(str): name of the email_subject setting used as a template.
        context(dict): context data to be substituted into the email subject.

    Returns:
        str: the rendered email subject.
    """
    subject_template = get_setting(setting_name, 'email_subject')

    template_renderer = Template(subject_template)
    con = RequestContext(request)
    con.push(context)
    rendered_email_subject = template_renderer.render(con)

    return rendered_email_subject


def get_email_greeting(recipients, adjective='Dear'):
    """Composes an email greeting to a list of Users.

    Args:
        recipients (list): ordered enumerable containing Users.
        adjective (str): the leading adjective to the greeting.

    Returns:
         str: an email greeting to one or more users.

    """
    recipient_forenames = [recipient.first_name for recipient in recipients]

    return '{adjective} {recipients}'.format(
        adjective=adjective,
        recipients=', '.join(
            filter(
                None,
                [', '.join(recipient_forenames[:-2])] +
                [' and '.join(recipient_forenames[-2:])]
            )
        )
    )


def file_path_book(book, attachment):
    return os.path.join(
        settings.BOOK_DIR,
        str(book.id),
        attachment.uuid_filename,
    )


def file_path_proposal(proposal, attachment):
    return os.path.join(
        settings.PROPOSAL_DIR,
        str(proposal.id),
        attachment.uuid_filename,
    )


def file_path_general(attachment):
    return os.path.join(
        settings.EMAIL_DIR,
        attachment.uuid_filename,
    )


class EmailSender():

    """
    This class represents an email object.
    Emails should be instantiated with email properties
    e.g. with a to address.
    """

    def __init__(
            self,
            to=None,
            from_address=None,
            cc=None,
            bcc=None,
            html_template=None,
            email_text=None,
            review_url=None,
            setting_name=None,
            setting_group_name=None,
            setting_default=None,
            sender=None,
            subject=None,
            kind=None,
            submission=None,
            attachment=None,
            attachments=None,
            copyedit=None,
            access_key=None,
            review=None,
            user=None,
            profile=None,
            assignment=None,
            owner=None,
            principal_contact_name=None,
            receiver=None,
            book=None,
            decision_url=None,
            editor=None,
            request=None,
            proposal=None,
            review_assignment=None,
            added_editors=None,
            removed_editors=None,
            index=None,
            typeset=None,
            revision=None,
            decision=None,
            code=None,
            due_date=None,
            notifications=None,
            notification_count=None,
            press_name=None,
    ):
        self.to = to
        self.from_address = from_address
        self.cc = cc
        self.bcc = bcc
        self.html_template = html_template
        self.email_text = email_text
        self.review_url = review_url
        self.kind = kind
        self.subject = subject
        self.setting_name = setting_name
        self.setting_group_name = setting_group_name
        self.setting_default = setting_default
        self.sender = sender
        self.kind = kind
        self.submission = submission
        self.attachment = attachment
        self.attachments = attachments
        self.copyedit = copyedit
        self.access_key = access_key
        self.review = review
        self.user = user
        self.profile = profile
        self.assignment = assignment
        self.owner = owner
        self.principal_contact_name = principal_contact_name
        self.receiver = receiver
        self.book = book
        self.decision_url = decision_url
        self.editor = editor
        self.request = request
        self.proposal = proposal
        self.review_assignment = review_assignment
        self.added_editors = added_editors
        self.removed_editors = removed_editors
        self.index = index
        self.typeset = typeset
        self.revision = revision
        self.decision = decision
        self.code = code
        self.due_date = due_date
        self.press_name = get_setting('press_name', 'general')
        self.base_url = get_setting('base_url', 'general')
        self.notifications = notifications
        self.notification_count = notification_count

        if self.to:
            if not isinstance(self.to, (list, tuple)):
                self.to = [self.to]

        if not self.from_address:
            if self.request and self.request.user.is_authenticated:
                self.from_address = "%s <%s>" % (
                    self.request.user.profile.full_name(),
                    self.request.user.email,
                )
            elif self.sender:
                self.from_address = self.sender.email
            elif self.request:
                self.from_address = self.request.user.email
            else:
                self.from_address = get_setting('from_address', 'email')

        if not subject:
            self.subject = get_setting(
                self.setting_name,
                self.setting_group_name,
                self.setting_default
            ) or ''

        self.context = {
            'author': self.typeset.book.owner if self.typeset else None,
            'editor': self.editor,
            'sender': self.sender,
            'submission': (
                self.typeset.book if self.typeset else None,
            ),
            'base_url': self.base_url,
            'review_url': self.review_url or None,
            'user': self.user or None,
            'profile': self.profile or None,
            'proposal': self.proposal or None,
            'copyedit': self.copyedit or None,
            'typeset': self.typeset or None,
            'press_name': self.press_name or None,
            'added_editors': self.added_editors or None,
            'removed_editors': (
                self.removed_editors or None,
            ),
            'assignment': self.assignment or None,
            'principal_contact_name': (
                self.principal_contact_name or None
            ),
            'review': self.review_assignment or self.review or None,
            'receiver': self.receiver or None,
            'owner': self.owner or None,
            'book': self.book or None,
            'decision_url': self.decision_url or None,
            'index': self.index or None,
            'revision': self.revision or None,
            'code': self.code or None,
            'review_assignment': (
                self.review_assignment or None,
            ),
            'due_date': self.due_date or None,
            'notifications': self.notifications or None,
            'notification_count': self.notification_count or None,
        }
        self.create_email_objects()
        self.msg = self.create_email_message()

        if self.access_key:
            # Hide access key in email log.
            self.email_text = self.email_text.replace(
                str(self.access_key), ''
            )

        self.log_kwargs = {
            'subject': self.subject,
            'from_address': self.from_address,
            'to': self.to or None,
            'bcc': self.bcc,
            'cc': self.cc,
            'content': self.html_content,
            'attachments': [self.attachment],
            'kind': self.kind,
        }

        if self.book:
            self.log_kwargs['book'] = self.book
        if self.proposal:
            self.log_kwargs['proposal'] = self.proposal

    def create_email_objects(self):
        """
        This method creates objects used by the email message.
        Seperated for testability.
        """
        html_template = Template(self.email_text.replace('\n', '<br>'))
        con = Context(self.context)
        self.html_content = html_template.render(con)

    def create_email_message(self):
        msg = create_email_message(
            self.subject,
            self.html_content,
            self.from_address,
            self.to or '',
            self.bcc,
            self.cc,
        )
        return msg

    def send_email(self):
        """
        This function:
        1. Adds attachment/s to message object.
        2. Sends email.
        3. Adds email log entry.
        """

        if isinstance(self.attachment, list) and self.proposal.id:
            for attachment in self.attachment:
                add_attachment(attachment, self.msg, self.proposal.id)
        elif self.attachment:
            if self.proposal:
                add_attachment(self.attachment, self.msg, self.proposal.id)
            else:
                add_attachment(self.attachment, self.msg)

        self.msg.send()

        log.add_email_log_entry_multiple(
            **self.log_kwargs
        )

    def send_email_multiple(self):
        if self.attachments:
            for attachment in self.attachments:
                add_attachment(attachment, self.msg)

        self.msg.send()

        log.add_email_log_entry_multiple(
            **self.log_kwargs
        )

    def send_reset_email(self, reset_code):

        """
        Subject line settings are:
        'reset_code_subject', 'email_subject', '[abp] Reset Code'
        to address is user.email
        """

        reset_url = 'http://{base_url}/login/reset/code/{reset_code}/'.format(
            base_url=self.base_url,
            reset_code=reset_code
        )

        self.context['reset_code'] = reset_code
        self.context['reset_url'] = reset_url

        self.create_email_objects()
        self.msg = self.create_email_message()

        self.send_email()

    def send_review_request(self):
        """
        Only being used once in editor.logic.
        to address is review_assignement.user.email
        """

        if isinstance(self.attachment, list) and self.proposal.id:
            for attachment in self.attachment:
                add_attachment(attachment, self.msg, self.proposal.id)
        elif self.attachment:
            if self.proposal:
                add_attachment(self.attachment, self.msg, self.proposal.id)
            else:
                self.html_content = template_renderer.render(con)
                add_attachment(self.attachment, self.msg)

        decision = 'decision'

        if self.access_key:
            decision = f'access_key/{self.access_key}/{decision}'

            decision_url = (
                'http://{base_url}/review/{review_type}/{book_id}/assignment/'
                '{assignment_id}/{decision}/'
            )

            self.context['decision_url'] = decision_url.format(
                base_url=self.base_url,
                review_type=self.review_assignment.review_type,
                book_id=self.book.id,
                assignment_id=self.review_assignment.id,
                decision=decision,
            )

            html_template = Template(self.email_text.replace('\n', '<br>'))
            con = Context(self.context)
            con.push(self.context)
            self.html_content = html_template.render(con)
            self.msg = self.create_email_message()

        self.send_email()

    def send_decision_ack(self):

        if self.decision == 'decline':
            decision_full = 'Reject Submission'
        else:
            decision_full = "Move to " + self.decision

        authors = self.book.author.all()

        self.kind = "submission"
        self.context['decision'] = self.decision
        self.context['submission'] = self.book
        self.context['link_to_page'] = self.decision_url

        self.subject = get_setting(
            'submission_decision_update_subject',
            'email_subject',
            'Submission decision update: %s' % decision_full
        )

        for author in authors:
            self.context['author'] = author
            self.to = author.author_email
            self.send_email()

    def send_editorial_review_request(self):
        pc_access_key = self.review_assignment.publishing_committee_access_key
        if self.review_assignment.publishing_committee_access_key:
            self.access_key = pc_access_key
        else:
            self.access_key = self.review_assignment.editorial_board_access_key

        url = 'http://%s/editorial/submission/%s/access_key/%s/'
        self.context['decision_url'] = url % (
            self.base_url,
            self.book.id,
            self.access_key,
        )

        for editor in self.review_assignment.editorial_board.all():
            self.to = editor.email
            self.send_email()

    def send_requests_revisions(self):
        url = "http://%s/author/submission/%s/revisions/%s"
        self.context['revision_url'] = url % (
            self.base_url,
            self.book.id,
            self.revision.id,
        )
        self.send_email_multiple()

    def send_proposal_review_request(self):
        if self.access_key:
            self.context['review_url'] = "http://{base_url}{url}".format(
                base_url=self.base_url,
                url=reverse(
                    'view_proposal_review_decision_access_key',
                    kwargs={
                        'proposal_id': self.proposal.id,
                        'assignment_id': self.review.id,
                        'access_key': self.access_key,
                    }
                )
            )
        else:
            self.context['review_url'] = "http://{0}{1}".format(
                self.base_url,
                reverse(
                    'view_proposal_review_decision',
                    kwargs={
                        'proposal_id': self.proposal.id,
                        'assignment_id': self.review.id,
                    }
                )
            )

        self.html_content = get_email_body(
            self.request,
            'proposal_review_request',
            {
                'sender': self.request.user,
                'review_url': self.context["review_url"],
                'due_date': self.context["due_date"],
                'press_name': self.context["press_name"],
                'review': self.review,
                'proposal': self.proposal,
                'book': self.book,
            }
        )
        self.msg = self.create_email_message()
        self.send_email()
