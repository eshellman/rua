import unittest
import factory
from factory.django import DjangoModelFactory

from revisions import models as revision_models

from core.email import EmailSender
from core.util import get_setting
from core import models as core_models


class BookFactory(DjangoModelFactory):

    class Meta:
        model = core_models.Book


class EditorialReviewAssignmentFactory(DjangoModelFactory):

    class Meta:
        model = core_models.EditorialReviewAssignment


class UserFactory(DjangoModelFactory):

    class Meta:
        model = 'auth.User'
        django_get_or_create = ('username',)

    username = 'test_user'

class EmailTests(unittest.TestCase):

    def setUp(self):

        self.book = BookFactory.create(
            title='test'
        )

        self.empty_review_assignment = core_models.ReviewAssignment(
            user_id=1,
            review_type='internal',
        )

        self.user = UserFactory.create(username='Preston')

        self.editorial_review_assignment = core_models.EditorialReviewAssignment(
            book_id=2,
            management_editor_id=1,
            publishing_committee_access_key='0000'
        )

        self.revision = revision_models.Revision(
            book_id=1,
            requestor_id=1,
        )

    def test_email_init(self):
        """
        Test correct attributes have been set on 
        object creation.
        """

        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.empty_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
        )

        self.assertEqual(email_sender.book.title, 'test')
        self.assertEqual(email_sender.to[0], 'editor@editing.com')
        self.assertEqual(email_sender.sender.username, 'Preston')
        self.assertEqual(email_sender.subject, 'Editorial Review Request')
        self.assertEqual(email_sender.log_kwargs['book'], self.book)
        self.assertEqual(email_sender.log_kwargs['subject'], email_sender.subject)

    def test_email_logic(self):
        """
        Make sure:
        1. 'to' addresses get turned into lists
        2. 'from email' is always set to sender.email when we have
        a sender.
        3. 'context' dictionary is has correct values.
        """
        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.empty_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
        )

        self.assertTrue(isinstance(email_sender.to, (list, tuple)))
        self.assertEqual(email_sender.from_address, email_sender.sender.email)
        self.assertEqual(email_sender.context['book'], email_sender.book)
        self.assertEqual(
            email_sender.context['review_assignment'][0],
            email_sender.review_assignment
        )
        self.assertEqual(email_sender.context['sender'], email_sender.sender)

    def test_email_mail_object(self):
        """
        Test that the email object creation method
        is run correctly when object is created.
        """

        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.empty_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
        )

        self.assertIn(
            'example email',
            email_sender.html_content,
        )

    def test_email_message_creation(self):
        """
        Test email message object is created
        when email object is created.
        """

        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.empty_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
        )

        self.assertEqual(email_sender.msg.from_email, 'webmaster@localhost')
        self.assertEqual(email_sender.msg.content_subtype, 'html')
        self.assertEqual(email_sender.msg.attachments, [])

    def test_send_review_request(self):
        """
        Test send_review_request method.
        """
        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.empty_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
            access_key='0000000',
        )

        email_sender.send_review_request()

        self.assertIn(
            'access_key/0000000/decision/',
            email_sender.context['decision_url'],
        )

    def test_send_decision_ack(self):
        """
        Test send_decision_ack method.
        """
        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.empty_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
            decision='decline',
        )

        email_sender.send_decision_ack()

        self.assertEqual(email_sender.context['decision'], email_sender.decision)
        self.assertIn('Submission decision', email_sender.subject)

    def test_send_requests_revisions(self):
        """
        Test send_requests_revisions method.
        """

        email_sender = EmailSender(
            book=self.book,
            to='editor@editing.com',
            review_assignment=self.editorial_review_assignment,
            email_text='example email',
            sender=self.user,
            setting_name='editorial_review_request',
            setting_group_name='email_subject',
            setting_default='Editorial Review Request',
            access_key='0000000',
            revision=self.revision,
        )

        email_sender.send_requests_revisions()

        self.assertIn('/author/submission/', email_sender.context['revision_url'])