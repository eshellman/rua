import random

from core import email
from core.util import get_setting


def generate_password():
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    pw_length = 8
    mypw = ""

    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        mypw = mypw + alphabet[next_index]

    return mypw


def send_new_user_ack(email_text, new_user, code):

    email_sender = email.EmailSender(
        setting_name='new_user_subject',
        setting_group_name='email_subject',
        setting_default='New User : Profile Details',
        to=new_user.email,
        email_text=email_text,
        kind='general',
        code=code,
    )
    email_sender.send_email()
