# -*- coding: utf-8 -*-
"""Django form widgets for use with the manager app."""

from django.forms.widgets import ClearableFileInput
from django.core.files import File
from core.models import File


class GeneratedFormClearableFileInput(ClearableFileInput):
    """A widget for dealing with file uploads to GeneratedForm classes.

    This widget is responsible for two things:
        1. Uploads of new files.
        2. Displaying filenames of existing files along with a 'clear' checkbox
           whose value is pulled directly from the POST dict of requests.
    """

    template_name = 'manager/widgets/generated_form_clearable_file_input.html'

    def value_from_datadict(self, data, files, name):
        upload = super().value_from_datadict(data, files, name)
        return upload if upload else data.get(name)

    def render(self, name, value, attrs=None, renderer=None):
        filename = None
        if value:
            filename = File.objects.get(pk=value).original_filename
        return super().render(name, filename, attrs, renderer)

    def is_initial(self, value):
        return bool(value)
